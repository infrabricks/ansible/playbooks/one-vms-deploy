# ONE VMs deploy

Ansible playbook to deploy VMs on OpenNebula.

## Requirements

* You need this role to use this playbook : [one_vms_deploy](https://gitlab.mim-libre.fr/infrabricks/ansible/roles/one-vms-deploy)
* Ansible >= 4

## OS

* Debian

## Playbook Example

An example of playbook

```
- name: Open Nebula deploy VMs
  hosts: localhost

  vars_files:
    - vars/main.yml
    - vars/vault.yml

  roles:
    - one_vms_deploy
```

## Author Information

* [Stéphane Paillet](mailto:spaillet@ethicsys.fr)
